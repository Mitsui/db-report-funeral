# QSSJ アナリティクス集計

Googleアナリティクスのレポート集計をテンプレートに従って自動化するライブラリです。

## 指標とディメンションについて

指標 と ディメンション は API Name で指定します。  
集計結果のヘッダーにも API Name がそのまま使われます。

指標・ディメンションの詳細については [**Dimensions & Metrics Explorer**](https://developers.google.com/analytics/devguides/reporting/core/dimsmets)(英語のみ) を参照してください。

### よく使う指標・ディメンション

よく使う指標・ディメンションをピックアップして解説します。

| API Name | UI Name | 備考 |
|:---|:---|:---|
| `ga:users` | ユーザー<br>(Users) | |
| `ga:sessions` | セッション<br>(Sessions) | ※注意:`ga:pagePath` でディメンションを区切って見る場合は、`ga:sessions` がカウントされるのは最初の1ページだけ|
| `ga:entrances` | 閲覧開始数<br>(Entrances) | `ga:sessions` と似ている。[閲覧開始数とセッション数の違い](https://support.google.com/analytics/answer/2956047?hl=ja) を参照してください。 |
| `ga:uniquePageviews` | ページ別訪問数<br>(ga:uniquePageviews) | ページ別の閲覧数を測るときは `ga:sessions` ではなく `ga:uniquePageviews` を使う |
| `ga:bounceRate` | 直帰率<br>(Bounce Rate) | |
| `ga:exitRate` | 離脱率<br>(% Exit) | |
| `ga:avgSessionDuration` | 平均セッション時間<br>(Avg. Session Duration) | 単位:秒数 |
| `ga:pageviewsPerSession` | ページ/セッション<br>(Pages/Session) | 1セッション毎のページビュー数 |
| `ga:goalCompletionsAll` | 目標の完了数<br>(Goal Conversions) | 全ての目標のコンバージョン数の合計 |
| `ga:goalConversionRateAll` | コンバージョン率<br>(Goal Conversion Rate) | 全ての目標を合算したコンバージョン率 |
| `ga:dayOfWeek` | 曜日<br>(Day of Week) | |
| `ga:hour` | 時<br>(Hour) | |
| `ga:yearMonth` | 月(年間)<br>(Month of Year) | |


## 集計の速度制限について

Googleアナリティクスへの問い合わせに(無料のプランでは)速度の制限があります。  
社内から二人以上が同時に集計を掛けた場合には、おそらく速度制限を超えてしまいエラーが出ます。

1回の集計に掛かる時間は2分以内なのでゆずり合ってご利用ください。


## レポートの設定について(デベロッパー向け)

集計するレポートの設定は PHP ファイルとして記述します。  
集計結果Excel の各タブは **/app/Report/SomeOfYourReport.php** 内のレポート設定用クラスに対応します。

各クラスは **/app/ReportSet.php** の中で列挙されて集計に使われています。  
ReportSet.php の中で列挙されないレポート設定用クラスは単に無視されて使われません。

### レポート設定の詳細

レポート設定用クラス内で設定できる値については **/app/Report/Example.php** を参照してください。  
また、[Core Reporting API - リファレンス ガイド](https://developers.google.com/analytics/devguides/reporting/core/v3/reference?hl=ja) も一読することをおすすめします。

また、レポート設定の組み立てのために集計を試す際には [Query Explorer](https://ga-dev-tools.appspot.com/query-explorer/) が便利です。
