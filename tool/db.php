<?php

class DB extends PDO
{
    public function __construct($dbpath)
    {
        parent::__construct("sqlite:$dbpath");

        // SQL実行時にもエラーの代わりに例外を投げるように設定
        // (毎回if文を書く必要がなくなる)
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // デフォルトのフェッチモードを連想配列形式に設定
        // (毎回PDO::FETCH_ASSOCを指定する必要が無くなる)
        $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }

    public function runSql($filename)
    {
        $sql = file_get_contents($filename);

        $this->exec($sql);

        return $this;
    }

    public function addFilters($site_id, $default = false)
    {
        $pages = [
            "all_top"             => "ga:pagePath=~^/($|\?),ga:pagePath=~^/sp/($|\?)",
            "all_fair_list"       => "ga:pagePath=~/fair/;ga:pagePath!~/fair/detail",
            "all_fair_detail"     => "ga:pagePath=~/fair/detail",
            "all_fair_index"      => "ga:pagePath=~/reserve_f/\?;ga:pagePath!~/reserve_f/.*confirm",
            "all_fair_confirm"    => "ga:pagePath=~/reserve_f/.*confirm",
            "all_fair_thanks"     => "ga:pagePath=~/reserve_f/thanks",
            "all_reserve_index"   => "ga:pagePath=~/reserve/$",
            "all_reserve_confirm" => "ga:pagePath=~/reserve/.*confirm",
            "all_reserve_thanks"  => "ga:pagePath=~/reserve/thanks",
            "all_inquiry_index"   => "ga:pagePath=~/inquiry/$",
            "all_inquiry_confirm" => "ga:pagePath=~/inquiry/.*confirm",
            "all_inquiry_thanks"  => "ga:pagePath=~/inquiry/thanks",
            "all_contact_index"   => "ga:pagePath=~/contact/$",
            "all_contact_confirm" => "ga:pagePath=~/contact/.*confirm",
            "all_contact_thanks"  => "ga:pagePath=~/contact/thanks",

            "pc_top"             => "ga:pagePath=~^/($|\?)",
            "pc_fair_list"       => "ga:pagePath=~^/fair/;ga:pagePath!~^/fair/detail",
            "pc_fair_detail"     => "ga:pagePath=~^/fair/detail",
            "pc_fair_index"      => "ga:pagePath=~^/reserve_f/\?;ga:pagePath!~^/reserve_f/.*confirm",
            "pc_fair_confirm"    => "ga:pagePath=~^/reserve_f/.*confirm",
            "pc_fair_thanks"     => "ga:pagePath=~^/reserve_f/thanks",
            "pc_reserve_index"   => "ga:pagePath=~^/reserve/$",
            "pc_reserve_confirm" => "ga:pagePath=~^/reserve/.*confirm",
            "pc_reserve_thanks"  => "ga:pagePath=~^/reserve/thanks",
            "pc_inquiry_index"   => "ga:pagePath=~^/inquiry/$",
            "pc_inquiry_confirm" => "ga:pagePath=~^/inquiry/.*confirm",
            "pc_inquiry_thanks"  => "ga:pagePath=~^/inquiry/thanks",
            "pc_contact_index"   => "ga:pagePath=~^/contact/$",
            "pc_contact_confirm" => "ga:pagePath=~^/contact/.*confirm",
            "pc_contact_thanks"  => "ga:pagePath=~^/contact/thanks",

            "sp_top"             => "ga:pagePath=~^/sp/($|\?)",
            "sp_fair_list"       => "ga:pagePath=~^/sp/fair/;ga:pagePath!~^/sp/fair/detail",
            "sp_fair_detail"     => "ga:pagePath=~^/sp/fair/detail",
            "sp_fair_index"      => "ga:pagePath=~^/sp/reserve_f/\?;ga:pagePath!~^/sp/reserve_f/.*confirm",
            "sp_fair_confirm"    => "ga:pagePath=~^/sp/reserve_f/.*confirm",
            "sp_fair_thanks"     => "ga:pagePath=~^/sp/reserve_f/thanks",
            "sp_reserve_index"   => "ga:pagePath=~^/sp/reserve/$",
            "sp_reserve_confirm" => "ga:pagePath=~^/sp/reserve/.*confirm",
            "sp_reserve_thanks"  => "ga:pagePath=~^/sp/reserve/thanks",
            "sp_inquiry_index"   => "ga:pagePath=~^/sp/inquiry/$",
            "sp_inquiry_confirm" => "ga:pagePath=~^/sp/inquiry/.*confirm",
            "sp_inquiry_thanks"  => "ga:pagePath=~^/sp/inquiry/thanks",
            "sp_contact_index"   => "ga:pagePath=~^/sp/contact/$",
            "sp_contact_confirm" => "ga:pagePath=~^/sp/contact/.*confirm",
            "sp_contact_thanks"  => "ga:pagePath=~^/sp/contact/thanks",

            "not_form" => "ga:pagePath!~(/reserve/|/reserve_f/|/inquiry/|/contact/)",
        ];

        $stmt = $this->prepare("
            INSERT INTO `page_filters`(`site_id`,`page`)
            VALUES (?, ?);
        ");

        $stmt_default = $this->prepare("
            INSERT INTO `page_filters`(`site_id`,`page`, `filters`)
            VALUES (?, ?, ?);
        ");

        foreach ($pages as $page => $filters) {
            if ($default) {
                $stmt_default->execute([
                    $site_id,
                    $page,
                    $filters,
                ]);
            } else {
                $stmt->execute([
                    $site_id,
                    $page,
                ]);
            }
        }
    }
}
