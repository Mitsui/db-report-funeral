CREATE TABLE
IF NOT EXISTS
`ga_responses` (
  `id`         INTEGER PRIMARY KEY,
  `view_id`    TEXT    NOT NULL,
  `blueprint`  TEXT    NOT NULL,
  `category`   TEXT,
  `query`      TEXT,
  `info`       TEXT,
  `results`    TEXT,
  `created_at` TEXT    DEFAULT CURRENT_TIMESTAMP
);
