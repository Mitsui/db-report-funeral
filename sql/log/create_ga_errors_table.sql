CREATE TABLE
IF NOT EXISTS
`ga_errors` (
  `id`         INTEGER PRIMARY KEY,
  `view_id`    TEXT    NOT NULL,
  `blueprint`  TEXT    NOT NULL,
  `category`   TEXT,
  `reason`     TEXT,
  `message`    TEXT,
  `errors`     TEXT,
  `created_at` TEXT    DEFAULT CURRENT_TIMESTAMP
);
