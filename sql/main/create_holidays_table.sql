CREATE TABLE
IF NOT EXISTS
`holidays` (
  `id`         INTEGER PRIMARY KEY,
  `date`       TEXT    NOT NULL,
  `name`       TEXT,
  `created_at` TEXT    DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TEXT    DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` TEXT
);
