CREATE TABLE
IF NOT EXISTS
`page_filters` (
  `id`         INTEGER PRIMARY KEY,
  `site_id`    INTEGER NOT NULL,
  `page`       TEXT    NOT NULL,
  `filters`    TEXT,
  `created_at` TEXT    DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TEXT    DEFAULT CURRENT_TIMESTAMP
);
