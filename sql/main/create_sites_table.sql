CREATE TABLE
IF NOT EXISTS
`sites` (
  `id`         INTEGER PRIMARY KEY,
  `name`       TEXT    NOT NULL,
  `url`        TEXT,
  `account_id` TEXT,
  `view_id`    TEXT    NOT NULL,
  `note`       TEXT,
  `created_at` TEXT    DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TEXT    DEFAULT CURRENT_TIMESTAMP
);
