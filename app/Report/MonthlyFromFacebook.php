<?php

namespace Stereotype\Report;

use Carbon\Carbon;

use Stereotype\Core\Blueprint;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Segment as SegmentLib;
use Stereotype\Library\Filters as FiltersLib;


class MonthlyFromFacebook extends Blueprint
{
    /**
     * 集計開始日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function startDate() {
        return Carbon::now()->subYears(2)->startOfMonth();
    }

    /**
     * 集計終了日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function endDate() {
        return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
    }

    /**
     * 指標(必須)
     *
     * @return array
     */
    public function metrics()
    {
        return [
            "ga:users",
            "ga:uniquePageviews",
        ];
    }

    /**
     * ディメンション
     *
     * @return array
     */
    public function dimensions()
    {
        return [
            "ga:yearMonth",
        ];
    }

    /**
     * フィルター
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return [
            "流入数" => "",
            "事前相談予約コンバージョン数" => $site->all_consultation_thanks_filters,
            "見積依頼コンバージョン数" => $site->all_estimate_thanks_filters,
            "来館予約コンバージョン数" => $site->all_reserve_thanks_filters,
            "お問い合わせコンバージョン数" => $site->all_inquiry_thanks_filters,
            "採用応募コンバージョン数" => $site->all_recruit_thanks_filters,
        ];
    }

    /**
     * セグメント
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return [
            "合計" => $lib->not_spam.";".$lib->from_facebook,
            "広告以外" => $lib->not_spam.";".$lib->from_facebook.";".$lib->from_not_ad,
            "広告のみ" => $lib->not_spam.";".$lib->from_facebook.";".$lib->from_ad,
        ];
    }
}
