<?php

namespace Stereotype\Report;

use Carbon\Carbon;

use Stereotype\Core\Blueprint;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Segment as SegmentLib;
use Stereotype\Library\Filters as FiltersLib;


class Example extends Blueprint
{
    /**
     * 集計開始日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function startDate() {
        return Carbon::now()->subYears(2)->startOfMonth();
    }

    /**
     * 集計終了日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function endDate() {
        return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
    }

    /**
     * 指標(必須)
     *
     * @return array
     */
    public function metrics()
    {
        return [
            "ga:users",
            "ga:sessions",
            "ga:bounceRate",
            "ga:avgSessionDuration",
            "ga:pageviewsPerSession",
        ];
    }

    /**
     * ディメンション
     *
     * @return array
     */
    public function dimensions()
    {
        return [];
    }

    /**
     * 集計結果の並び順のデフォルト
     *
     * @return array
     */
    public function sort()
    {
        return [];
    }

    /**
     * フィルター
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return "";
    }

    /**
     * セグメント
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return $lib->not_spam;
    }

    /**
     * 集計結果取得数
     *
     * @return int
     */
    public function maxResults()
    {
        return 1000;
    }

    /**
     * 集計後に実行されるコールバック
     *
     * @var    array $row  集計結果の1行
     * @var    array $rows 集計結果全体
     * @return array|null
     */
    public function callback(array $row, array $rows)
    {
        return $row;
    }
}
