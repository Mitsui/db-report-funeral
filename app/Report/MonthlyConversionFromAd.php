<?php

namespace Stereotype\Report;

use Carbon\Carbon;

use Stereotype\Core\Blueprint;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Segment as SegmentLib;
use Stereotype\Library\Filters as FiltersLib;


class MonthlyConversionFromAd extends Blueprint
{
    /**
     * 集計開始日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function startDate() {
        return Carbon::now()->subYears(2)->startOfMonth();
    }

    /**
     * 集計終了日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function endDate() {
        return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
    }

    /**
     * 指標(必須)
     *
     * @return array
     */
    public function metrics()
    {
        return [
            "ga:users",
        ];
    }

    /**
     * ディメンション
     *
     * @return array
     */
    public function dimensions()
    {
        return [
            "ga:yearMonth",
        ];
    }

    /**
     * フィルター
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return array_filter([
            "PC+SP 事前相談予約" => $site->all_consultation_thanks_filters,
            "PC+SP 見積依頼" => $site->all_estimate_thanks_filters,
            "PC+SP 来館予約" => $site->all_reserve_thanks_filters,
            "PC+SP お問い合わせ" => $site->all_inquiry_thanks_filters,
            "PC+SP 採用応募" => $site->all_recruit_thanks_filters,

            "PC 事前相談予約" => $site->pc_consultation_thanks_filters,
            "PC 見積依頼" => $site->pc_estimate_thanks_filters,
            "PC 来館予約" => $site->pc_reserve_thanks_filters,
            "PC お問い合わせ" => $site->pc_inquiry_thanks_filters,
            "PC 採用応募" => $site->pc_recruit_thanks_filters,

            "SP 事前相談予約" => $site->sp_consultation_thanks_filters,
            "SP 見積依頼" => $site->sp_estimate_thanks_filters,
            "SP 来館予約" => $site->sp_reserve_thanks_filters,
            "SP お問い合わせ" => $site->sp_inquiry_thanks_filters,
            "SP 採用応募" => $site->sp_recruit_thanks_filters,
        ]);
    }

    /**
     * セグメント
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return $lib->not_spam.";".$lib->from_ad;
    }
}
