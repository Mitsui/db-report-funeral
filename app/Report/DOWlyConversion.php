<?php

namespace Stereotype\Report;

use Carbon\Carbon;

use Stereotype\Core\Blueprint;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Segment as SegmentLib;
use Stereotype\Library\Filters as FiltersLib;


class DOWlyConversion extends Blueprint
{
    /**
     * 集計開始日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function startDate() {
        return Carbon::now()->subMonths(6)->startOfMonth();
    }

    /**
     * 集計終了日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function endDate() {
        return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
    }

    /**
     * 指標(必須)
     *
     * @return array
     */
    public function metrics()
    {
        return [
            "ga:goalCompletionsAll",
            "ga:goalConversionRateAll",
        ];
    }

    /**
     * ディメンション
     *
     * @return array
     */
    public function dimensions()
    {
        return [
            "ga:dayOfWeek",
        ];
    }

    /**
     * フィルター
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return $lib->not_callcv.";".$lib->in_not_lp;
    }

    /**
     * セグメント
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return [
            "PC+SP" => $lib->not_spam.";".$lib->from_not_ad,
            "PC"    => $lib->not_spam.";".$lib->from_not_ad.";".$lib->use_pc,
            "SP"    => $lib->not_spam.";".$lib->from_not_ad.";".$lib->use_sp,
        ];
    }
}
