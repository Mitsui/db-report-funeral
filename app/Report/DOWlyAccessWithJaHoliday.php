<?php

namespace Stereotype\Report;

use Carbon\Carbon;

use Stereotype\Core\Blueprint;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Segment as SegmentLib;
use Stereotype\Library\Filters as FiltersLib;


class DOWlyAccessWithJaHoliday extends Blueprint
{
    /**
     * 集計開始日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function startDate() {
        return Carbon::now()->subMonths(6)->startOfMonth();
    }


    /**
     * 集計終了日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function endDate() {
        return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
    }

    /**
     * 指標(必須)
     *
     * @return array
     */
    public function metrics()
    {
        return [
            "ga:users",
            "ga:sessions",
            "ga:bounceRate",
            "ga:avgSessionDuration",
            "ga:pageviewsPerSession",
        ];
    }

    /**
     * フィルター
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        $start_date = $this->startDate();
        $end_date = $this->endDate();

        $days = $start_date->diffInDays($end_date);

        $sundays    = [];
        $mondays    = [];
        $tuesdays   = [];
        $wednesdays = [];
        $thursdays  = [];
        $fridays    = [];
        $saturdays  = [];
        $holidays   = [];

        for ($i = 0; $i <= $days; $i++) {
            $date = $start_date->copy()->addDays($i);

            if ($calendar->isHoliday($date)) {
                $holidays[] = $i;
                continue;
            } else if (Carbon::SUNDAY === $date->dayOfWeek) {
                $sundays[] = $i;
                continue;
            } else if (Carbon::MONDAY === $date->dayOfWeek) {
                $mondays[] = $i;
                continue;
            } else if (Carbon::TUESDAY === $date->dayOfWeek) {
                $tuesdays[] = $i;
                continue;
            } else if (Carbon::WEDNESDAY === $date->dayOfWeek) {
                $wednesdays[] = $i;
                continue;
            } else if (Carbon::THURSDAY === $date->dayOfWeek) {
                $thursdays[] = $i;
                continue;
            } else if (Carbon::FRIDAY === $date->dayOfWeek) {
                $fridays[] = $i;
                continue;
            } else if (Carbon::SATURDAY === $date->dayOfWeek) {
                $saturdays[] = $i;
                continue;
            }
        }

        $this->dow_count = [
            "日曜" => count($sundays),
            "月曜" => count($mondays),
            "火曜" => count($tuesdays),
            "水曜" => count($wednesdays),
            "木曜" => count($thursdays),
            "金曜" => count($fridays),
            "土曜" => count($saturdays),
            "祝日" => count($holidays),
        ];

        return [
            "日曜" => self::toFiltersString($sundays),
            "月曜" => self::toFiltersString($mondays),
            "火曜" => self::toFiltersString($tuesdays),
            "水曜" => self::toFiltersString($wednesdays),
            "木曜" => self::toFiltersString($thursdays),
            "金曜" => self::toFiltersString($fridays),
            "土曜" => self::toFiltersString($saturdays),
            "祝日" => self::toFiltersString($holidays),
        ];
    }

    /**
     * セグメント
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return $lib->not_spam.";".$lib->from_not_ad;
    }

    /**
     * 集計後に実行されるコールバック
     *
     * @var    array $row  集計結果の1行
     * @var    array $rows 集計結果全体
     * @return array|null
     */
    public function callback(array $row, array $rows)
    {
        $category = $row["category"];
        $dow_count = $this->dow_count[$category];

        $users = 0 === $dow_count ? (string)0 : $row["ga:users"] / $dow_count;
        $sessions = 0 === $dow_count ? (string)0 : $row["ga:sessions"] / $dow_count;

        $new_row = [
            "曜日" => $row["category"],
            "平均ユーザー数" => $users,
            "平均セッション数" => $sessions,
            "直帰率" => $row["ga:bounceRate"],
            "平均セッション時間" => $row["ga:avgSessionDuration"],
            "ページビュー/セッション" => $row["ga:pageviewsPerSession"],
        ];

        return $new_row;
    }


    private static function toFiltersString($nth_days)
    {
        $nth_dayss = array_chunk($nth_days, 20);

        $filter_strings = array_map(function($nth_days) {
            $regexp = self::toRegexp($nth_days);

            return "ga:nthDay=~$regexp";
        }, $nth_dayss);

        return join(",", $filter_strings);
    }

    private static function toRegexp(array $nth_days)
    {
        $nth_days = array_map(function($nth_day) {
            return sprintf('%04d', $nth_day);
        }, $nth_days);

        return "^".join("|", $nth_days)."$";
    }
}
