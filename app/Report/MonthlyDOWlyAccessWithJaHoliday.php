<?php

namespace Stereotype\Report;

use Carbon\Carbon;

use Stereotype\Core\Blueprint;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Segment as SegmentLib;
use Stereotype\Library\Filters as FiltersLib;


class MonthlyDOWlyAccessWithJaHoliday extends Blueprint
{
    /**
     * 集計開始日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function startDate() {
        return Carbon::now()->subYears(2)->startOfMonth();
    }

    /**
     * 集計終了日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function endDate() {
        return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
    }

    /**
     * 指標(必須)
     *
     * @return array
     */
    public function metrics()
    {
        return [
            "ga:users",
            "ga:sessions",
            "ga:bounceRate",
            "ga:avgSessionDuration",
            "ga:pageviewsPerSession",
        ];
    }

    /**
     * ディメンション
     *
     * @return array
     */
    public function dimensions()
    {
        return [
            "ga:yearMonth",
        ];
    }

    /**
     * フィルター
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        $start_date = $this->startDate();
        $end_date = $this->endDate();

        $days = $start_date->diffInDays($end_date);

        $weekdays    = [];
        $preholidays = [];
        $holidays    = [];

        $weekdays_count    = [];
        $preholidays_count = [];
        $holidays_count    = [];

        for ($i = 0; $i <= $days; $i++) {
            $date = $start_date->copy()->addDays($i);

            // 各日数のカウントのため、変数を初期化。
            $date_str = $date->format("Y/m");
            if (!isset($weekdays_count[$date_str])) {
                $weekdays_count[$date_str] = 0;
            }
            if (!isset($preholidays_count[$date_str])) {
                $preholidays_count[$date_str] = 0;
            }
            if (!isset($holidays_count[$date_str])) {
                $holidays_count[$date_str] = 0;
            }

            if (self::isPreholiday($calendar, $date)) {
                $preholidays[] = $i;
                $preholidays_count[$date_str] += 1;
                continue;
            } else if (self::isHoliday($calendar, $date)) {
                $holidays[] = $i;
                $holidays_count[$date_str] += 1;
                continue;
            } else {
                $weekdays [] = $i;
                $weekdays_count[$date_str] += 1;
                continue;
            }
        }

        $this->weekdays_count    = $weekdays_count;
        $this->preholidays_count = $preholidays_count;
        $this->holidays_count    = $holidays_count;

        return [
            "平日" => self::toFiltersString($weekdays),
            "土日祝日の前日" => self::toFiltersString($preholidays),
            "土日祝日" => self::toFiltersString($holidays),
        ];
    }

    /**
     * セグメント
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return $lib->not_spam.";".$lib->from_not_ad;
    }

    /**
     * 集計後に実行されるコールバック
     *
     * @var    array $row  集計結果の1行
     * @var    array $rows 集計結果全体
     * @return array|null
     */
    public function callback(array $row, array $rows)
    {
        $category = $row["category"];
        $year_month = $row["ga:yearMonth"];

        if ("平日" == $category) {
            $days_count = $this->weekdays_count[$year_month];
        } else if ("土日祝日の前日" == $category) {
            $days_count = $this->preholidays_count[$year_month];
        } else if ("土日祝日" == $category) {
            $days_count = $this->holidays_count[$year_month];
        }

        $users = $row["ga:users"] / $days_count;
        $sessions = $row["ga:sessions"] / $days_count;

        return [
            "曜日" => $category,
            "年月" => $year_month,
            "平均ユーザー数" => (string)$users,
            "平均セッション数" => (string)$sessions,
            "直帰率" => $row["ga:bounceRate"],
            "平均セッション時間" => $row["ga:avgSessionDuration"],
            "ページビュー/セッション" => $row["ga:pageviewsPerSession"],
        ];
    }


    private static function toFiltersString($nth_days)
    {
        $nth_dayss = array_chunk($nth_days, 20);

        $filter_strings = array_map(function($nth_days) {
            $regexp = self::toRegexp($nth_days);

            return "ga:nthDay=~$regexp";
        }, $nth_dayss);

        return join(",", $filter_strings);
    }

    private static function toRegexp(array $nth_days)
    {
        $nth_days = array_map(function($nth_day) {
            return sprintf('%04d', $nth_day);
        }, $nth_days);

        return "^".join("|", $nth_days)."$";
    }

    private static function isPreholiday(Calendar $calendar, Carbon $date)
    {
        $next = $date->copy()->addDay();

        return self::isWeekday($calendar, $date)
               && self::isHoliday($calendar, $next);
    }

    private static function isWeekday(Calendar $calendar, Carbon $date)
    {
        return $is_holiday = $date->isWeekday() && !$calendar->isHoliday($date);
    }

    private static function isHoliday(Calendar $calendar, Carbon $date)
    {
        return $is_holiday = $date->isWeekend() || $calendar->isHoliday($date);
    }
}
