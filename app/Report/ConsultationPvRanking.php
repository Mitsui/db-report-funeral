<?php

namespace Stereotype\Report;

use Carbon\Carbon;

use Stereotype\Core\Blueprint;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Segment as SegmentLib;
use Stereotype\Library\Filters as FiltersLib;


class consultationPvRanking extends Blueprint
{
    /**
     * 集計開始日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function startDate() {
        return Carbon::now()->subMonths(2)->startOfMonth();
    }

    /**
     * 集計終了日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function endDate() {
        return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
    }

    /**
     * 指標(必須)
     *
     * @return array
     */
    public function metrics()
    {
        return [
            "ga:uniquePageviews",
        ];
    }

    /**
     * ディメンション
     *
     * @return array
     */
    public function dimensions()
    {
        return [
            "ga:pagePath",
        ];
    }

    /**
     * 集計結果の並び順のデフォルト
     *
     * @return array
     */
    public function sort()
    {
        return [
            "-ga:uniquePageviews",
        ];
    }

    /**
     * フィルター
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return "ga:pagePath=~/fair/detail\.";
    }

    /**
     * セグメント
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return [
            "コンバージョンしたユーザー" => $lib->not_spam.";".$lib->from_not_ad.";".$lib->converters,
            "全てのユーザー"   => $lib->not_spam.";".$lib->from_not_ad,
        ];
    }

    /**
     * 集計後に実行されるコールバック
     *
     * @var    array $row  集計結果の1行
     * @var    array $rows 集計結果全体
     * @return array|null
     */
    public function callback(array $row, array $rows)
    {
        if (!isset($this->fair_ids)) { $this->fair_ids = []; }

        $path = $row["ga:pagePath"];
        $fair_id = self::getFairId($path);

        if (in_array($fair_id, $this->fair_ids)) {
            return null;
        } else {
            $this->fair_ids[] = $fair_id;
        }

        $pv = self::sumPv($fair_id, $rows);
        $conversion_rate = 0 === $pv["allusers_pv"] ? 0 : $pv["converters_pv"] / $pv["allusers_pv"];

        return [
            "フェアID" => (string)$fair_id,
            "PV数(全てのユーザー)" => (string)$pv["allusers_pv"],
            "PV数(コンバージョンしたユーザー)" => (string)$pv["converters_pv"],
            "コンバージョン率" => (string)$conversion_rate,
        ];
    }


    private static function getFairId($path)
    {
        if (preg_match("/id=(\d{4})/", $path, $m)) {
            return $m[1];
        } else {
            return null;
        }
    }

    private static function sumPv($fair_id, array $rows)
    {
        $converters_pv = 0;
        $allusers_pv = 0;

        foreach ($rows as $row) {
            $path = $row["ga:pagePath"];

            if ($fair_id == self::getFairId($path)) {
                if ("コンバージョンしたユーザー" === $row["category"]) {
                    $converters_pv += intval($row["ga:uniquePageviews"]);
                } else {
                    $allusers_pv += intval($row["ga:uniquePageviews"]);
                }
            } else {
                continue;
            }
        }

        return compact("converters_pv", "allusers_pv");
    }
}
