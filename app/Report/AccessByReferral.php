<?php

namespace Stereotype\Report;

use Carbon\Carbon;

use Stereotype\Core\Blueprint;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Segment as SegmentLib;
use Stereotype\Library\Filters as FiltersLib;


class AccessByReferral extends Blueprint
{
    /**
     * 集計開始日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function startDate() {
        return Carbon::now()->subMonths(6)->startOfMonth();
    }

    /**
     * 集計終了日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function endDate() {
        return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
    }

    /**
     * 指標(必須)
     *
     * @var array
     */
    public function metrics()
    {
        return [
            "ga:users",
            "ga:sessions",
            "ga:bounceRate",
            "ga:avgSessionDuration",
            "ga:pageviewsPerSession",
            "ga:goalCompletionsAll",
            "ga:goalConversionRateAll",
        ];
    }

    /**
     * フィルター
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return [
            "直アクセス" => $lib->direct,
            "検索" => $lib->from_organic_search,
            "広告" => $lib->from_ad,
            "SNS+Blog" => $lib->from_not_ad.";".$lib->from_sns.",".$lib->from_blog,
            "その他" => $lib->not_direct
                      .";".$lib->from_not_organic_search
                      .";".$lib->from_not_ad
                      .";".$lib->from_not_sns
                      .";".$lib->from_not_blog
        ];
    }

    /**
     * セグメント
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return [
            "PC+SP" => $lib->not_spam.";".$lib->from_not_ad,
            "PC"    => $lib->not_spam.";".$site->view_pc_site_segment,
            "SP"    => $lib->not_spam.";".$site->view_sp_site_segment,
        ];
    }
}
