<?php

namespace Stereotype\Report;

use Carbon\Carbon;

use Stereotype\Core\Blueprint;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Segment as SegmentLib;
use Stereotype\Library\Filters as FiltersLib;


class HourlyDOWlyConversionAlt extends Blueprint
{
    /**
     * 集計開始日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function startDate() {
        return Carbon::now()->subMonths(6)->startOfMonth();
    }

    /**
     * 集計終了日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function endDate() {
        return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
    }

    /**
     * 指標(必須)
     *
     * @return array
     */
    public function metrics()
    {
        return [
            "ga:uniquePageviews",
        ];
    }

    /**
     * ディメンション
     *
     * @return array
     */
    public function dimensions()
    {
        return [
            "ga:dayOfWeek",
            "ga:hour",
        ];
    }

    /**
     * フィルター
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return array_filter([
            "PC+SP" => $lib->not_callcv.";".$lib->in_not_lp.";".$site->all_consultation_thanks_filters.",".$site->all_estimate_thanks_filters.",".$site->all_reserve_thanks_filters.",".$site->all_inquiry_thanks_filters.",".$site->all_recruit_thanks_filters,
            "PC"    => $lib->not_callcv.";".$lib->in_not_lp.";".$site->pc_consultation_thanks_filters.",".$site->pc_estimate_thanks_filters.",".$site->pc_reserve_thanks_filters.",".$site->pc_inquiry_thanks_filters.",".$site->pc_recruit_thanks_filters,
            "SP"    => $lib->not_callcv.";".$lib->in_not_lp.";".$site->sp_consultation_thanks_filters.",".$site->sp_estimate_thanks_filters.",".$site->sp_reserve_thanks_filters.",".$site->sp_inquiry_thanks_filters.",".$site->sp_recruit_thanks_filters,
        ]);
    }

    /**
     * セグメント
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return $lib->not_spam.";".$lib->from_not_ad;
    }
}
