<?php

namespace Stereotype;

use Stereotype\Core\Core;
use Stereotype\Report;


class ReportSet extends Core
{
    public $filename = "AnalyticsReport";

    public function blueprint()
    {
        return [
            "月別アクセス数"
                => new Report\MonthlyAccess,
            "月別コンバージョン"
                => new Report\MonthlyConversion,
            "月別アクセス数(広告のみ)"
                => new Report\MonthlyAccessFromAd,
            "月別コンバージョン(広告のみ)"
                => new Report\MonthlyConversionFromAd,

            "時間別アクセス数"
                => new Report\HourlyAccess,
            "時間別コンバージョン率"
                => new Report\HourlyConversion,
            "時間別コンバージョン数(5フォーム)"
                => new Report\HourlyConversionAlt,
            "曜日別アクセス数"
                => new Report\DOWlyAccess,
            "曜日別コンバージョン率"
                => new Report\DOWlyConversion,
            "曜日別コンバージョン数(5フォーム)"
                => new Report\DOWlyConversionAlt,
            "時間別×曜日別アクセス数"
                => new Report\HourlyDOWlyAccess,
            "時間別×曜日別コンバージョン率"
                => new Report\HourlyDOWlyConversion,
            "時間別×曜日別コンバージョン数(5フォーム)"
                => new Report\HourlyDOWlyConversionAlt,

            "事前相談予約フォーム遷移"
                => new Report\ConsultationTransition,
            "見積依頼フォーム遷移"
                => new Report\EstimateTransition,
            "来館予約フォーム遷移"
                => new Report\ReserveTransition,
            "お問い合わせフォーム遷移"
                => new Report\InquiryTransition,
            "採用応募フォーム遷移"
                => new Report\RecruitTransition,
            "月別事前相談予約フォーム遷移"
                => new Report\MonthlyConsultationTransition,
            "月別見積依頼フォーム遷移"
                => new Report\MonthlyEstimateTransition,
            "月別来館予約フォーム遷移"
                => new Report\MonthlyReserveTransition,
            "月別お問い合わせフォーム遷移"
                => new Report\MonthlyInquiryTransition,
            "月別採用応募フォーム遷移"
                => new Report\MonthlyRecruitTransition,

            "リファラー別アクセス数"
                => new Report\AccessByReferral,
            "月別×リファラー別アクセス数"
                => new Report\MonthlyAccessByReferral,
            "月別Facebookからのアクセス数"
                => new Report\MonthlyFromFacebook,

            "祝日込み曜日別アクセス数"
                => new Report\DOWlyAccessWithJaHoliday,
            "祝日込み曜日別コンバージョン率"
                => new Report\DOWlyConversionWithJaHoliday,
            "月別×祝日込み曜日別アクセス数"
                => new Report\MonthlyDOWlyAccessWithJaHoliday,
            "月別×祝日込み曜日別コンバージョン率"
                => new Report\MonthlyDOWlyConversionWithJaHoliday,

            "コンバージョン寄与率ランキング"
                => new Report\PercentContributionRanking,
            "事前相談予約PVランキング"
                => new Report\ConsultationPvRanking,

            "全期間のアクセス数"
                => new Report\Access,
            "全期間のコンバージョン数"
                => new Report\Conversion,
            "全期間のアクセス数(広告のみ)"
                => new Report\AccessFromAd,
            "全期間のコンバージョン数(広告のみ)"
                => new Report\ConversionFromAd,
        ];
    }
}
