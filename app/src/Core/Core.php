<?php

namespace Stereotype\Core;

use Stereotype\Util\Util as U;

use PHPExcel;
use PHPExcel_IOFactory;

use ProgressBar\Manager as ProgressBar;

use Stereotype\DB\Site;
use Stereotype\DB\Log;

use Stereotype\Service\Analytics;
use Stereotype\Service\Batch;

use Stereotype\Response\Response;

use Stereotype\Core\Summary;


class Core
{
    /**
     * Google Analytics サービスアカウント
     *
     * @var \Stereotype\Service\Analytics
     */
    private $analytics;


    /**
     * ログ保存用データベース
     *
     * @var \Stereotype\DB\Log
     */
    private $log;

    /**
     * 集計対象のビューID
     *
     * @var string
     */
    private $view_id;

    /**
     * 集計対象サイトのModel
     *
     * @var \Stereotype\DB\Site
     */
    private $site;

    /**
     * 集計する全てのブループリントのセット
     *
     * @var array
     */
    private $blueprints;

    /**
     * 集計結果
     *
     * @var array
     */
    private $summarys;


    public function __construct(Analytics $analytics, $view_id = null)
    {
        date_default_timezone_set("Asia/Tokyo");

        $this->analytics = $analytics;

        $this->log = new Log;

        if (!is_null($view_id)) {
            // 第2引数で $view_id が与えられていればそれをそのまま使う

            $this->view_id = $view_id;

            $this->site = new Site($view_id);
        } else {
            // 与えられていなければコマンドライン引数のキーワードを元にDBから検索する

            global $argv;

            if (!isset($argv[1])) {
                throw new \Exception("Any of Account ID, View ID and URL is not given.");
            }

            $keyword = $argv[1];

            $site = Site::createFromKeyword($keyword);

            if (!is_null($site)) {
                // キーワードからのSiteオブジェクトの生成に成功すればそれを使う

                $this->view_id = $site->getViewId();

                $this->site = $site;
            } else if (preg_match("/\d+/", $keyword)) {
                // Siteオブジェクト生成に失敗しても $keyword が数字のみで構成されていれば
                // それを $view_id だと信じて処理を続行する

                $this->view_id = $keyword;

                $this->site = new Site($keyword);
            } else {
                // どちらでもなければエラー投げる

                throw new \Exception("Couldn't find View ID.");
            }
        }

        $this->blueprints = $this->blueprint();

        $this->summarys = [];
    }


    /**
     * 集計を実行
     *
     * @return self
     */
    public function run()
    {
        set_time_limit(600);

        if (U::is_cli()) {
            $name = $this->site->getName();
            $report_count = count($this->blueprints);

            $view_id = $this->view_id;
            $account_id = $this->site->getAccountId();

            U::out("「${name}」の集計を開始します。");
            U::br();

            U::out("アカウントID: UA-${account_id}");
            U::out("ビューID: ga:${view_id}");
            U::out("集計レポート: 全 ${report_count} 種類");
            U::br();

            U::out("データを集計中...");

            // プログレスバーを生成
            $progress = new ProgressBar(0, $report_count);
        }

        foreach ($this->blueprints as $summary_name => $blueprint) {
            // ブループリントのカテゴリーを展開
            $querys = $blueprint->expand($this->site);


            // 全てのクエリ―をバッチリクエスト
            $batch = new Batch($this->analytics);
            foreach ($querys as $request_name => $query) {
                $batch->add($request_name, $query);
            }
            $responses = $batch->execute();


            if (U::is_cli()) {
                // プログレスバーを進める
                $progress->advance();
            }

            // レスポンスからサマリーを生成
            $summary = (new Response(
                    $this->log,
                    $responses,
                    $querys
                ))
                ->getSummary()
                ->process([$blueprint, "callback"]);


            $this->summarys[$summary_name] = $summary;
        }

        return $this;
    }


    /**
     * 集計結果をエクセル形式で保存
     *
     * @return array
     */
    public function save($filename = null)
    {
        if (U::is_cli()) {
            U::br();
            U::out("集計結果を保存中...");

            // プログレスバーを生成
            $progress = new ProgressBar(0, count($this->summarys) + 1);
        }


        $filename = is_null($filename) ? $this->filename : $filename;

        $book = new PHPExcel();

        $summarys = $this->summarys;
        $summary_names = array_keys($summarys);


        // 目次タブ作成
        $index_sheet = $book->getSheet(0);
        $index_sheet->getDefaultStyle()->getFont()->setName("Arial");
        $index_sheet->setTitle("目次");
        $index_sheet->getColumnDimension("A")->setWidth(50);

        $index_sheet->getCell("A1")
            ->setValue("目次");

        $index_sheet->getStyle("A1")
            ->applyFromArray([
                "font" => [
                    "size" => 14,
                ],
            ]);

        if (U::is_cli()) {
            // プログレスバーを進める
            $progress->advance();
        }


        foreach ($summary_names as $index => $tabname) {
            $row = $index + 3;
            $index_sheet->getCell("A${row}")
                ->setValue("- $tabname")
                ->getHyperlink()->setUrl("sheet://'${tabname}'!A1");

            $index_sheet->getStyle("A${row}")
                ->applyFromArray([
                    "font" => [
                        "color" => [
                            "rgb" => "0000FF",
                        ],
                        "underline" => "single",
                    ],
                ]);

            $index_sheet->getRowDimension($row)
                ->setRowHeight(20);

            if (U::is_cli()) {
                // プログレスバーを進める
                $progress->advance();

                usleep(60000);
            }
        }


        // 各タブを生成してデータの貼り込み
        foreach ($summarys as $summary_name => $summary) {
            $sheet = $book->createSheet();
            $sheet->getDefaultStyle()->getFont()->setName("Arial");
            $sheet->setTitle($summary_name);
            $sheet->fromArray($summary->toCSV());
        }

        $filename = $filename . ".xlsx";
        if (U::is_windows_os() && U::is_cli()) {
            $savename = U::to_sjis($filename);
        } else {
            $savename = $filename;
        }

        $writer = PHPExcel_IOFactory::createWriter($book, "Excel2007");
        $writer->save($savename);


        if (U::is_cli()) {
            U::br();
            U::out("集計結果を \"$filename\" に保存しました。");
        }


        return compact("filename");
    }

    /**
     * ブループリント設定のデフォルト
     *
     * @return array
     */
    protected function blueprint()
    {
        return [];
    }
}
