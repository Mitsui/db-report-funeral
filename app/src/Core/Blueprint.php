<?php

namespace Stereotype\Core;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Filters as FiltersLib;
use Stereotype\Library\Segment as SegmentLib;


class Blueprint
{
    /**
     * サマリー名
     *
     * @return string
     */
    private $name;


    public function __construct()
    {
        // 呼び出し元のクラス名を取得
        $ref = new \ReflectionClass($this);
        $namespaces = explode("\\", $ref->getName());

        $this->name = array_pop($namespaces);
    }


    /**
     * デフォルトの callback
     *
     * @var    array      $row  集計結果の1行
     * @var    array      $rows 集計結果の全体
     * @return array|null
     */
    public function callback(array $row, array $rows)
    {
        return $row;
    }


    /**
     * セグメントとフィルターの各カテゴリーを展開して複数のクエリを生成
     *
     * @var    srting $view_id
     * @return array
     */
    public function expand($site)
    {
        $calendar    = new Calendar;
        $view_id     = $site->getViewId();
        $site        = $site;
        $filters_lib = new FiltersLib;
        $segment_lib = new SegmentLib;

        $start_date  = $this->startDate();
        $end_date    = $this->endDate();
        $metrics     = $this->metrics();
        $dimensions  = $this->dimensions();
        $sort        = $this->sort();
        $filters     = $this->filters($filters_lib, $calendar, $site);
        $segment     = $this->segment($segment_lib, $calendar, $site);
        $max_results = $this->maxResults();


        $querys = [
            $this->getName() => new Query(compact(
                "view_id",
                "start_date",
                "end_date",
                "metrics",
                "dimensions",
                "sort",
                "max_results"
            )),
        ];


        $new_querys = [];
        if (is_array($filters)) {
            // $filters が配列の場合は展開
            foreach ($querys as $name => $query) {
                foreach ($filters as $category => $an_filters) {
                    $new_name = $name."__".$category;

                    $new_query = $query->clone([
                        "filters" => $an_filters,
                    ]);

                    $new_querys[$new_name] = $new_query;
                }
            }
        } else {
            // 単一のフィルターの場合はそのまま各クエリに適用
            foreach ($querys as $name => $query) {
                $new_query = $query->clone([
                    "filters" => $filters,
                ]);

                $new_querys[$name] = $new_query;
            }
        }
        $querys = $new_querys;


        $new_querys = [];
        if (is_array($segment)) {
            // $segment が配列の場合は展開
            foreach ($querys as $name => $query) {
                foreach ($segment as $category => $an_segment) {
                    $new_name = $name."__".$category;

                    $new_query = $query->clone([
                        "segment" => $an_segment,
                    ]);

                    $new_querys[$new_name] = $new_query;
                }
            }
        } else {
            // 単一のセグメントの場合はそのまま各クエリに適用
            foreach ($querys as $name => $query) {
                $new_query = $query->clone([
                    "segment" => $segment,
                ]);

                $new_querys[$name] = $new_query;
            }
        }
        $querys = $new_querys;


        return $querys;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @return \Stereotype\DB\Calendar
     */
    public function getCalendar()
    {
        return $this->calendar;
    }


    /**
     * @return \Stereotype\DB\Sites
     */
    public function getSites()
    {
        return $this->sites;
    }


    /**
     * 集計開始日のデフォルト
     *
     * @return \Carbon\Carbon
     */
    public function startDate()
    {
        throw new \Exception("'start-date' is not set.");

        return Carbon::now()->subYears(2)->startOfMonth();
    }

    /**
     * 集計終了日のデフォルト
     *
     * @return \Carbon\Carbon
     */
    public function endDate()
    {
        throw new \Exception("'end-date' is not set.");

        return Carbon::now()->subMonth()->endOfMonth();
    }

    /**
     * 指標のデフォルト
     *
     * @return array
     */
    public function metrics()
    {
        throw new \Exception("'metrics' is not set.");

        return [
            "ga:users",
        ];
    }

    /**
     * ディメンションのデフォルト
     *
     * @return array
     */
    public function dimensions()
    {
        return [];
    }

    /**
     * 集計結果の並び順のデフォルト
     *
     * @return array
     */
    public function sort()
    {
        return [];
    }

    /**
     * フィルターのデフォルト
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return "";
    }

    /**
     * セグメントのデフォルト
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return "";
    }

    /**
     * 集計結果取得数のデフォルト
     *
     * @return int
     */
    public function maxResults()
    {
        return 1000;
    }
}
