<?php

namespace Stereotype\Service;

use Google_Client;
use Google_Service_Analytics;
use Google_Auth_AssertionCredentials;

use Stereotype\Util\Config;


class Client extends Google_Client
{
    const HOST = "localhost:8000";

    /**
     * 設定ファイルから読み込んだクライアント設定
     *
     * @var array
     */
    private $config;


    public static function createServiceClient()
    {
        $client = new self;

        $config = $client->initConfig();

        $key = file_get_contents(__DIR__."/../../".$config->get("key"));
        $cred = new Google_Auth_AssertionCredentials(
            $config->get("client_id"),
            [Google_Service_Analytics::ANALYTICS_READONLY],
            $key
        );

        $client->setApplicationName($config->get("app_name"));
        $client->setAssertionCredentials($cred);

        if ($client->getAuth()->isAccessTokenExpired()) {
          $client->getAuth()->refreshTokenWithAssertion($cred);
        }

        return $client;
    }

    public static function createWebAppClient()
    {
        $client = new self;

        $config = $client->initConfig();

        $client->setAuthConfigFile(__DIR__."/../../".$config->get("auth_config_file"));
        $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);

        return $client;
    }


    public function initConfig()
    {
        $this->config = new Config(__DIR__."/../../config.yml");

        return $this->config;
    }

    public function createAuthUrl()
    {
        $this->setRedirectUri("http://".self::HOST."/auth");

        return parent::createAuthUrl();
    }

    public function authenticate($code, $crossClient = false)
    {
        parent::authenticate($code, $crossClient);
        $_SESSION["access_token"] = $this->getAccessToken();

        return $this;
    }
}
