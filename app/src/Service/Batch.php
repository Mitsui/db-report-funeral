<?php

namespace Stereotype\Service;

use Carbon\Carbon;

use Stereotype\Service\Analytics;
use Stereotype\Core\Query;


class Batch
{
    /**
     * Analytics API の割当数
     *
     * @var int
     */
    const RequestsPer100secondsPerUser = 1000;

    /**
     * 分割リクエスト1回分のリクエスト数
     *
     * @var int
     */
    const Chunk = 10;

    /**
     * Google API の割当限度に対しての安全率
     *
     * @var float
     */
    const SafeLimmit = 0.8;


    /**
     * Google Analytics サービスアカウント
     *
     * @var \Service\Analytics
     */
    private $analytics;

    /**
     * リクエスト格納用のキュー
     *
     * @var array
     */
    private $queue;


    public function __construct(Analytics &$analytics)
    {
        $this->analytics = $analytics;
        $this->analytics->getClient()->setUseBatch(true);

        $this->queue = [];
    }


    /**
     * サマリーからリスエストを組み立ててバッチに追加
     *
     * @var    string                 $name    リクエスト名
     * @var    \Stereotype\Core\Query $query   クエリ
     * @var    string                 $view_id ビューID
     * @return self
     */
    public function add(
        $name,
        Query $query
    )
    {
        // マルチバイト文字を入れ込んだ場合に対処する為
        // リクエスト名をURLエンコード
        $encoded_name = urlencode($name);

        $view_id    = $query->getViewId();
        $start_date = $query->getStartDate();
        $end_date   = $query->getEndDate();
        $metrics    = $query->getMetrics();
        $options    = $this->buildOptions($query);

        $request = $this->analytics->get(
            $view_id,
            $start_date,
            $end_date,
            $metrics,
            $options
        );

        $this->queue[$encoded_name] = $request;

        return $this;
    }

    /**
     * リスエストのオプション設定を組み立て
     *
     * @var    \Stereotype\Core\Query $query クエリ
     * @return array
     */
    private function buildOptions(Query $query)
    {
        $options = [];


        if ($query->hasDimensions()) {
            $options["dimensions"] = join(",", $query->getDimensions());
        }

        if ($query->hasSort()) {
          $options["sort"] = join(",", $query->getSort());
        }

        if ($query->hasFilters()) {
            $options["filters"] = $query->getFilters();
        }

        if ($query->hasSegment()) {
            $options["segment"] = $query->getSegment();
        }

        if ($query->hasMaxResults()) {
            $options["max-results"] = $query->getMaxResults();
        }


        return $options;
    }

    /**
     * バッチの中のリクエストを一括問い合わせ
     * リクエストは self::Chunk の数で分割されて実行される
     *
     * @return array
     */
    public function execute()
    {
        $chunked_queue = array_chunk($this->queue, self::Chunk, true);
        $all_response = [];

        foreach ($chunked_queue as $chunk) {
            usleep(self::Chunk * 100000000 / self::RequestsPer100secondsPerUser / self::SafeLimmit);

            $batch = $this->analytics->createBatch();

            foreach ($chunk as $name => $query) {
                $batch->add($query, $name);
            }

            $response = $batch->execute();
            $all_response = array_merge($all_response, $response);
        }

        // URLエンコードされていたリクエスト名を元に戻す
        $decoded_response = [];
        foreach ($all_response as $name => $response) {
            $decoded_name = urldecode($name);

            $decoded_response[$decoded_name] = $response;
        }

        return $decoded_response;
    }
}
