<?php

namespace Stereotype\Util;

use Carbon\Carbon;


class Processer
{
    /**
     * Google Analytics から返される割合を表す数が何故か2桁シフトしているので補正
     * 42.5 -> 0.425
     *
     * @var    string $data
     * @return string
     */
    public static function percent($data)
    {
        return (string)(floatval($data) / 100);
    }

    /**
     * 年 と 月 をスラッシュで区切って正規化
     * "201510" -> "2015/10/01"
     *
     * @var    string $data
     * @return string
     */
    public static function year_month($data)
    {
        return substr_replace($data, '/', 4, 0);
    }

    /**
     * 時 を正規化
     * "10" -> "10:00:00"
     *
     * @var    string $data
     * @return string
     */
    public static function hour($data)
    {
        return "$data:00:00";
    }

    /**
     * 秒 を 秒と分 に分けて正規化
     * "100" -> "00:01:40"
     *
     * @var    string $data
     * @return string
     */
    // public static function second($data)
    // {
    //     $seconds = floatval($data);
    //
    //     $minutes = floor($seconds / 60);
    //     $seconds = $seconds % 60;
    //
    //     $hours = floor($minutes / 60);
    //     $minutes = $minutes % 60;
    //
    //     return "$hours:$minutes:$seconds";
    // }

    /**
     * 曜日のインデックスを日本語名に変換
     * 0 -> "日曜"
     *
     * @var    string $data
     * @return string
     */
    public static function ja_dow($data)
    {
        $dows = [
            0 => "日曜",
            1 => "月曜",
            2 => "火曜",
            3 => "水曜",
            4 => "木曜",
            5 => "金曜",
            6 => "土曜",
        ];

        $dow_index = intval($data);

        return $dows[$dow_index];
    }

    /**
     * 曜日のインデックスを英語名に変換
     * 0 -> "Sun"
     *
     * @var    string $data
     * @return string
     */
    public static function en_dow($data)
    {
        $dows = [
            0 => "Sun",
            1 => "Mon",
            2 => "Tue",
            3 => "Wed",
            4 => "Thu",
            5 => "Fri",
            6 => "Sat",
        ];

        $dow_index = intval($data);

        return $dows[$dow_index];
    }
}
