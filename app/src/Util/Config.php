<?php

namespace Stereotype\Util;

use Symfony\Component\Yaml\Yaml;


class Config
{
    private $config;

    public function __construct($yaml_file)
    {
        if (!file_exists($yaml_file)) {
            throw new \InvalidArgumentException('File does not exist.');
        }

        $this->config =  Yaml::parse(file_get_contents($yaml_file));
    }

    public function get($name, $default_value = null)
    {
        if (isset($this->config[$name])) {
            return $this->config[$name];
        } else if (!is_null($default_value)) {
            return $default_value;
        } else {
            throw new \Exception("Config value not found.");
        }
    }

    public function has($name)
    {
        return isset($this->config[$name]);
    }
}
