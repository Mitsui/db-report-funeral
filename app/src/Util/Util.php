<?php

namespace Stereotype\Util;


class Util
{
    /**
     * 環境に依って適切な文字コードに変換しつつ echo する
     *
     * @var    string $str 任意の文字コードのテキスト
     * @return void
     */
    public static function out($str)
    {
        $str = self::is_windows_os() ? self::to_sjis($str) : self::to_utf8($str);

        echo $str . PHP_EOL;
    }

    /**
     * Line Break
     *
     * @return void
     */
    public static function br()
    {
        echo PHP_EOL;
    }


    /**
     * 文字コードを Shift_JIS に変換
     *
     * @var    string $str 任意の文字コードのテキスト
     * @return string
     */
    public static function to_sjis($str)
    {
        return mb_convert_encoding(
            $str,
            "SJIS-win",
            "ASCII,JIS,UTF-8,eucJP-win,SJIS-win"
        );
    }

    /**
     * 文字コードを UTF-8 に変換
     *
     * @var    string $str 任意の文字コードのテキスト
     * @return string
     */
    public static function to_utf8($str)
    {
        return mb_convert_encoding(
            $str,
            "UTF-8",
            "ASCII,JIS,UTF-8,eucJP-win,SJIS-win"
        );
    }

    /**
     * 実行環境が Windows かどうか判別
     *
     * @return bool
     */
    public static function is_windows_os()
    {
        return DIRECTORY_SEPARATOR == "\\";
    }

    /**
     * コマンドラインでの実行かどうか判別
     *
     * @return bool
     */
    public static function is_cli()
    {
        return php_sapi_name() == "cli";
    }
}
