<?php

namespace Stereotype\Response;

use Stereotype\DB\Log as LogDB;
use Stereotype\Core\Summary;

class Response
{
    /**
     * データベースオブジェクト
     *
     * @var \Database\Database
     */
    private $log_db;

    /**
     * API からの生のレスポンス
     *
     * @var array
     */
    private $responses;

    /**
     * レスポンスを元に生成したサマリー
     *
     * @var \Stereotype\Core\Summary
     */
    private $summary;


    public function __construct(LogDB &$log_db, array $responses, array $querys)
    {
        $this->log_db = $log_db;

        $this->responses = $responses;

        // 空のサマリーを生成
        $summary = new Summary();

        // サマリーにデータを追加
        foreach ($querys as $request_name => $query) {
            $category = self::getCategory($request_name);

            $response_name = "response-$request_name";
            $ga_data = $responses[$response_name];

            if ("Google_Service_Exception" === get_class($ga_data)) {
                // エラーをDBに保存
                $errors = $ga_data->getErrors()[0];

                $this->log_db->insert_ga_error(
                    $query->getViewId(),
                    $request_name,
                    $category,
                    $errors
                );

                // var_dump($ga_data->getErrors);

                throw new \Exception($errors["reason"]);
            } else {
                // レスポンスをDBに保存
                $this->log_db->insert_ga_response(
                    $query->getViewId(),
                    $request_name,
                    $category,
                    $ga_data
                );

                $summary->add($category, $ga_data);
            }
        }

        $this->summary = $summary;
    }


    /**
     * レスポンスの key を分解してカテゴリー名を取得
     *
     * @var    string $name レスポンスの key
     * @return array
     */
    private static function getCategory($request_name)
    {
        // "__" 区切りになっている ブループリント名 と カテゴリー名 を分割
        $exploded = explode("__", $request_name);

        // カテゴリー名が無い場合(セグメントにもフィルタにもバリエーションが無い場合)に対処
        if (count($exploded) >= 2) {
            array_shift($exploded);
            return join("-", $exploded);
        } else {
            return $request_name;
        }
    }

    /**
     * getter: $summary
     *
     * @return Stereotype\Core\Summary
     */
    public function getSummary()
    {
        return $this->summary;
    }
}
