<?php

namespace Stereotype\DB;

use PDO;


class DB extends PDO
{
    public function __construct($dbpath)
    {
        parent::__construct("sqlite:$dbpath");

        // SQL実行時にもエラーの代わりに例外を投げるように設定
        // (毎回if文を書く必要がなくなる)
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // デフォルトのフェッチモードを連想配列形式に設定
        // (毎回PDO::FETCH_ASSOCを指定する必要が無くなる)
        $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }
}
