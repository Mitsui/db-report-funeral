<?php

namespace Stereotype\DB;

use Google_Service_Analytics_GaData;


class Log extends DB
{
    public function __construct()
    {
        parent::__construct(__DIR__."/../../database/log.sqlite");
    }

    /**
     * Google Analytics API からの正常なレスポンスのログを保存
     *
     * @var    string $view_id
     * @var    string $blueprint_name
     * @var    string $category
     * @var    \Google_Service_Analytics_GaData $response
     * @return bool
     */
    public function insert_ga_response(
        $view_id,
        $blueprint_name,
        $category,
        Google_Service_Analytics_GaData $response
    )
    {
        if (!isset($this->stmt_insert_ga_response)) {
            $this->stmt_insert_ga_response = $this->prepare("
                INSERT INTO
                `ga_responses`(`view_id`, `blueprint`, `category`, `query`, `info`, `results`)
                VALUES (?, ?, ?, ?, ?, ?)
            ");
        }

        try {
            $query   = $response->getQuery();
            $info    = $response->getProfileInfo();
            $results = $response->getTotalResults();

            $this->stmt_insert_ga_response->execute([
                $view_id,
                $blueprint_name,
                $category,
                json_encode($query),
                json_encode($info),
                json_encode($results),
            ]);

            return true;
        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;

            return false;
        }
    }

    /**
     * Google Analytics API からの正常なレスポンスのログを保存
     *
     * @var    string $view_id
     * @var    string $blueprint_name
     * @var    string $category
     * @var    $errors
     * @return bool
     */
    public function insert_ga_error(
        $view_id,
        $blueprint_name,
        $category,
        $errors
    )
    {
        if (!isset($this->stmt_insert_ga_error)) {
            $this->stmt_insert_ga_error = $this->prepare("
                INSERT INTO
                `ga_errors`(`view_id`, `blueprint`, `category`, `reason`, `message`, `errors`)
                VALUES (?, ?, ?, ?, ?, ?)
            ");
        }

        try {
            $reason  = $errors["reason"];
            $message = $errors["message"];

            $this->stmt_insert_ga_error->execute([
                $view_id,
                $blueprint_name,
                $category,
                $reason,
                $message,
                json_encode($errors),
            ]);

            return true;
        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;

            return false;
        }
    }

    public function getGaErrors()
    {
        if (!isset($this->stmt_get_ga_errors)) {
            $this->stmt_get_ga_errors = $this->prepare("
                SELECT
                    `created_at`,
                    `view_id`,
                    `blueprint`,
                    `category`,
                    `reason`,
                    `message`,
                    `errors`
                FROM `ga_errors`
                ORDER BY `created_at` DESC
                LIMIT 10
            ");
        }

        try {
            $stmt = $this->stmt_get_ga_errors;

            $stmt->execute([]);
            $results = $stmt->fetchAll();

            return $results;
        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;

            return false;
        }
    }
}
