<?php

namespace Stereotype\DB;

class Main extends DB
{
    public function __construct()
    {
        parent::__construct(__DIR__."/../../database/main.sqlite");
    }

    public function find_from_sites($id)
    {
        if (!isset($this->stmt_find_from_sites)) {
            $this->stmt_find_from_sites = $this->prepare("
                SELECT `id`, `name`, `url`, `account_id`, `view_id`
                FROM `sites`
                WHERE `id` = ?
            ");
        }

        $results = $this->stmt_find_from_sites->execute([$id]);

        if (empty($results)) { return null; }

        return array_combine(
            ["id", "name", "url", "account_id", "view_id"],
            $result[0]
        );
    }
}
