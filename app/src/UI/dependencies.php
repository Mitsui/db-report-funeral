<?php

use Stereotype\Service\Client;
use Stereotype\Service\Analytics;


// DIC configuration
$container = new \Slim\Container($settings);

$container['notFoundHandler'] = function ($c) {
    return new \StereotypeUI\Action\DefaultAction($c->response);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings');
    $logger = new Monolog\Logger($settings['logger']['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['logger']['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Google API PHP Client
$container["client"] = function($c) {
    return Client::createWebAppClient();
};

// Analytics API
$container["analytics"] = function($c) {
    return new Analytics($c["client"]);
};

// middleware
$container["StereotypeUI\Middleware\AuthMiddleware"] = function ($c) {
    return new \StereotypeUI\Middleware\AuthMiddleware($c->client);
};

return $container;
